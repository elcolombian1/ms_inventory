package com.ms.ms_inventory

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MsInventoryApplication

fun main(args: Array<String>) {
	runApplication<MsInventoryApplication>(*args)
}
